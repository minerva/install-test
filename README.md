The only purpose of this project is to test documentation install.sh script in different environments.

|System|Status|
|---|---|
|Debian Stretch|![Debian Stretch status](https://git-r3lab.uni.lu/minerva/install-test/-/jobs/artifacts/master/raw/test_status/badge.svg?job=test_debian_stretch)|
|Debian Jessie|![Debian Jessie status](https://git-r3lab.uni.lu/minerva/install-test/-/jobs/artifacts/master/raw/test_status/badge.svg?job=test_debian_jessie)|
|Ubuntu 18.04|![Ubuntu 18.04 status](https://git-r3lab.uni.lu/minerva/install-test/-/jobs/artifacts/master/raw/test_status/badge.svg?job=test_ubuntu_18)|
|Ubuntu 16.04|![Ubuntu 16.04 status](https://git-r3lab.uni.lu/minerva/install-test/-/jobs/artifacts/master/raw/test_status/badge.svg?job=test_ubuntu_16)|
|Ubuntu 14.04|![Ubuntu 14.04 status](https://git-r3lab.uni.lu/minerva/install-test/-/jobs/artifacts/master/raw/test_status/badge.svg?job=test_ubuntu_14)|


