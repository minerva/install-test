#!/bin/bash

set -e
directory='test_status'
mkdir $directory

cp badge_fail.svg $directory/badge.svg

DEBIAN_FRONTEND=noninteractive
apt-get update
apt-get install -y curl lsb-release locales
lsb_release -a > $directory/info.txt

#make sure that we have locale defined
sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen 
dpkg-reconfigure --frontend=noninteractive locales 
update-locale LANG=en_US.UTF-8

echo debconf shared/accepted-oracle-license-v1-1 select true | debconf-set-selections
#hacky way of installing minerva on docker - by default rc-invoke is preventing services changes on docker
mv /usr/sbin/policy-rc.d /usr/sbin/policy-rc.d.backup
mv gitlab-ci-policy-rc.d /usr/sbin/policy-rc.d
chmod 0755 /usr/sbin/policy-rc.d

bash <(curl -s https://minerva.pages.uni.lu/doc/scripts/install.sh)

mv /usr/sbin/policy-rc.d.backup /usr/sbin/policy-rc.d

set +e
TOMCAT_PACKAGE="";
TOMCAT7_OK=$(dpkg-query -W --showformat='${Status}\n' tomcat7|grep "install ok installed")
TOMCAT8_OK=$(dpkg-query -W --showformat='${Status}\n' tomcat8|grep "install ok installed")
if [ "$TOMCAT7_OK" != "" ];
then
  TOMCAT_PACKAGE='tomcat7'
fi
if [ "$TOMCAT8_OK" != "" ];
then
  TOMCAT_PACKAGE='tomcat8'
fi
set -e


service $TOMCAT_PACKAGE start
#we need to wait a bit for tomcat start
sleep 15
wget http://localhost:8080/minerva/

#test if we can login and get configuration data
test 200 = $(curl --retry 0 --write-out %{http_code} --silent --output /dev/null -c cookie.txt http://localhost:8080/minerva/api/doLogin)
test 200 = $(curl --retry 0 --write-out %{http_code} --silent --output configuration.json --cookie cookie.txt http://localhost:8080/minerva/api/configuration/)

cp badge_pass.svg $directory/badge.svg
