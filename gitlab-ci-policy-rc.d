#!/bin/sh

# For most Docker users, "apt-get install" only happens during "docker build",
# where starting services doesn't work and often fails in humorous ways. This
# prevents those failures by stopping the services from attempting to start.

echo "policy-rc.d for " $1

if [ "$1" = "tomcat7" ] || [ "$1" = "tomcat8" ];
then
        exit 101
fi

exit 0;

