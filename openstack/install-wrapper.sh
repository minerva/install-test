#!/bin/bash
echo debconf minerva/internal/skip-preseed boolean true | debconf-set-selections
echo debconf minerva/dbconfig-install select true | debconf-set-selections
echo debconf minerva/pgsql/app-pass select 123qweasdzxc | debconf-set-selections
echo debconf minerva/remote/host select localhost | debconf-set-selections
echo debconf minerva/db/dbname select map_viewer | debconf-set-selections
echo debconf minerva/db/app-user select map_viewer@localhost |debconf-set-selections
echo debconf minerva/database-type select pgsql | debconf-set-selections
echo debconf minerva/pgsql/manualconf boolean false | debconf-set-selections
curl -s https://minerva.pages.uni.lu/doc/scripts/install.sh -o minerva-install.sh;
sudo bash minerva-install.sh;
rm minerva-install.sh
